https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain

echo "test" > test.txt

## SHA-1 hash
git hash-object test.txt
30d74d258442c7c65512eafab474568dd706c430

## Inna tresc -> Inny SHA-1 hash
git hash-object -w test.txt
30d74d258442c7c65512eafab474568dd706c430

<!-- git\objects\30\d74d258442c7c65512eafab474568dd706c430 -->
<!-- text -->

git cat-file -p 30d74d258442c7c65512eafab474568dd706c430
<!-- test -->

git cat-file -p 30d7 > test.txt
<!-- test -->

git hash-object -w katalog/inny.txt
30d74d258442c7c65512eafab474568dd706c430

<!-- Poczekalnia -->
git update-index --add --cacheinfo 100644 30d74d258442c7c65512eafab474568dd706c430 test.txt
git write-tree
<!-- 095a057d4a651ec412d06b59e32e9b02871592d5 -->
git read-tree 095a057d4a651ec412d06b59e32e9b02871592d5

git update-index --add --cacheinfo 100644 f3a34851d44d6b97c90fbb99dd3d18c261b9a237 test2.txt
<!-- fa89759570feb222288c92af908b87fa9c48afd1 -->
git read-tree --prefix=podkatalog fa89759570feb222288c92af908b87fa9c48afd1
git write-tree
<!-- b151b1fddcefea123890e4686691ce0555d73021 -->

<!-- Commit -->
git commit-tree b151b1fddcefea123890e4686691ce0555d73021 -m "First Commit"
<!-- cd0b9f3aed5e539fd49086f82c4315198c4ecfbd -->
git cat-file -p cd0b9f3aed5e539fd49086f82c4315198c4ecfbd
<!-- tree b151b1fddcefea123890e4686691ce0555d73021
author Mateusz Kulesza <mateusz@altkom.pl> 1650623411 +0200   
committer Mateusz Kulesza <mateusz@altkom.pl> 1650623411 +0200
 -->

 <!-- second comit -->
 git add .
 git write-tree
 <!-- d13b9838240c9c907327d0d1c07ec04e7a45110d -->
 git cat-file -p d13b9838240c9c907327d0d1c07ec04e7a45110d
<!-- 
100644 blob e3ce0b363ec9ca1c8e6fdf3cb0e827cb15b7695b    nowyplik.txt
100644 blob cd5218e73a87cb52a3f01b41f85a562d623dd076    plumbing.md
040000 tree fa89759570feb222288c92af908b87fa9c48afd1    podkatalog
100644 blob 6295b7232f30958cae1a267008e523bb03b10dfc    test.txt 
-->

<!-- New comint with old as PARENT -->
git commit-tree d13b9838240c9c907327d0d1c07ec04e7a45110d -m "Second Commit" -p cd0b9f3aed5e539fd49086f82c4315198c4ecfbd
<!-- 891e5bef3b7fb59baa9c5dcf900f4137c5e05f0e -->
git cat-file -p 891e5bef3b7fb59baa9c5dcf900f4137c5e05f0e
<!-- 
tree d13b9838240c9c907327d0d1c07ec04e7a45110d  
parent cd0b9f3aed5e539fd49086f82c4315198c4ecfbd
author Mateusz Kulesza <mateusz@altkom.pl> 1650624201 +0200
committer Mateusz Kulesza <mateusz@altkom.pl> 1650624201 +0200

Second Commit
 -->

git reset --hard 891e5bef3b7fb59baa9c5dcf900f4137c5e05f0e

 git log --oneline
<!-- 891e5be (HEAD -> kopia) Second Commit
cd0b9f3 (master) First Commit -->
